﻿using System.Collections.Generic;
using com.adjust.sdk;
using com.adjust.sdk.purchase;
using UnityEngine;
using UnityEngine.Events;

public class AdjustPurchaseSDKManager : MonoBehaviour {

    #region Custom Variables

    [System.Serializable]
    public class AdjustRevenueTracker {
#if UNITY_EDITOR

        public bool showOnEditor = true;
        

#endif

        public string uniqueIdentifier = "";
        public string eventTokenForAndroid = "{EventToken = Android}";
        public string eventTokenForIOS = "{EventToken = iOS}";
        public string currency = "USA";
        public double purchaseValue = 1;
        public TransactionId GetTransactionId;

        [System.Serializable]
        public class TransactionId : SerializableCallback<string> { }
    }

    private class PurchasedItemInfo {

        public int eventId;

        #region Private Variables

        private bool isSandboxTesting;
        private string name;
        private string uniqueTransactionId;
        private string currency;
        private double purchaseValue;
        private UnityAction<int> OnEventCycleComplete;
        private UnityAction OnPurchasedSuccessful;
        private UnityAction OnPurchasedFailed;
        private UnityAction<string> OnPassingLogEvent;

        #endregion

        #region Configuretion

        private void VerificationInfoDelegate (ADJPVerificationInfo verificationInfo) {

            string t_LogMessage = "None";

            switch (verificationInfo.VerificationState) {
                case ADJPVerificationState.ADJPVerificationStatePassed:

                    AdjustEvent t_NewAdjustSuccessfulEvent = new AdjustEvent ("PurchasedSuccessfully_" + name);
                    t_NewAdjustSuccessfulEvent.setRevenue (purchaseValue, currency);
                    t_NewAdjustSuccessfulEvent.setTransactionId (uniqueTransactionId);
                    Adjust.trackEvent (t_NewAdjustSuccessfulEvent);

                    t_LogMessage = "PurchasedSuccessful -> '" + name + "'" +
                        ". Message : " + verificationInfo.Message +
                        ", Status Code : " + verificationInfo.StatusCode +
                        ", Verification State : " + verificationInfo.VerificationState;

                    if (isSandboxTesting) {
                        Debug.Log (t_LogMessage);
                    }

                    OnPurchasedSuccessful?.Invoke ();

                    break;
                case ADJPVerificationState.ADJPVerificationStateFailed:

                    AdjustEvent t_NewAdjustFailedEvent = new AdjustEvent ("PurchasedFailed_" + name);
                    Adjust.trackEvent (t_NewAdjustFailedEvent);

                    t_LogMessage = "PurchasedFailed -> '" + name + "'" +
                        ". Message : " + verificationInfo.Message +
                        ", Status Code : " + verificationInfo.StatusCode +
                        ", Verification State : " + verificationInfo.VerificationState;

                    if (isSandboxTesting) {
                        Debug.Log (t_LogMessage);
                    }

                    OnPurchasedFailed?.Invoke ();

                    break;
                case ADJPVerificationState.ADJPVerificationStateNotVerified:

                    AdjustEvent t_NewAdjustNotVerifiedEvent = new AdjustEvent ("PurchasedNotVerified_" + name);
                    Adjust.trackEvent (t_NewAdjustNotVerifiedEvent);

                    t_LogMessage = "PurchasedNotVerified -> '" + name + "'" +
                        ". Message : " + verificationInfo.Message +
                        ", Status Code : " + verificationInfo.StatusCode +
                        ", Verification State : " + verificationInfo.VerificationState;

                    if (isSandboxTesting) {
                        Debug.Log (t_LogMessage);
                    }

                    OnPurchasedFailed?.Invoke ();

                    break;
                case ADJPVerificationState.ADJPVerificationStateUnknown:

                    AdjustEvent t_NewAdjustUnknownEvent = new AdjustEvent ("PurchasedUnknown_" + name);
                    Adjust.trackEvent (t_NewAdjustUnknownEvent);

                    t_LogMessage = "PurchasedNotVerified -> '" + name + "'" +
                        ". Message : " + verificationInfo.Message +
                        ", Status Code : " + verificationInfo.StatusCode +
                        ", Verification State : " + verificationInfo.VerificationState;

                    if (isSandboxTesting) {
                        Debug.Log (t_LogMessage);
                    }

                    OnPurchasedFailed?.Invoke ();

                    break;
            }

            OnPassingLogEvent?.Invoke (t_LogMessage);
            OnEventCycleComplete.Invoke (eventId);
        }

        #endregion

        public PurchasedItemInfo (
            int eventId,
            bool isSandboxTesting,
            string name,
            string uniqueTransactionId,
            string currency,
            double purchaseValue,
            UnityAction<int> OnEventCycleComplete,
            UnityAction OnPurchasedSuccessful,
            UnityAction OnPurchasedFailed,
            UnityAction<string> OnPassingLogEvent) {

            this.eventId = eventId;
            this.isSandboxTesting = isSandboxTesting;
            this.name = name;
            this.uniqueTransactionId = uniqueTransactionId;
            this.currency = currency;
            this.purchaseValue = purchaseValue;
            this.OnEventCycleComplete = OnEventCycleComplete;
            this.OnPurchasedSuccessful = OnPurchasedSuccessful;
            this.OnPurchasedFailed = OnPurchasedFailed;
            this.OnPassingLogEvent = OnPassingLogEvent;
        }

        public void VerifyPurchaseForiOS (string t_Recepit, string t_TransactionId, string t_ProductId) {

            AdjustPurchase.VerifyPurchaseiOS (t_Recepit, t_TransactionId, t_ProductId, VerificationInfoDelegate);
        }

        public void VerifyPurchaseForAndroid (string t_ItemSKU, string t_ItemToken, string t_DeveloperPayLoad) {

            AdjustPurchase.VerifyPurchaseAndroid (t_ItemSKU, t_ItemToken, t_DeveloperPayLoad, VerificationInfoDelegate);
        }
    }

    #endregion

    #region Public Variables

    public static AdjustPurchaseSDKManager Instance;

#if UNITY_EDITOR

    public bool updateAdjustConfiguretion;

    public bool updateAdjustPurchaseConfiguretion;
    public bool showAdjustPurchaseConfiguretionHelpBox;
    public bool showAdjustPurchaseConfiguretion;
    public bool showAdvanceAdjustPurchaseConfiguretion;

    public bool updateEnumListForPreConfigRevenueEventTracker;
    public bool showAdjustPreConfigRevenueEventTrackerHelpBox;
    public bool showAdjustPreConfigRevenueEventTracker;


#endif

    public string appTokenForAndroid;
    public string appTokenForIOS;

    public bool isProductionBuild = false;
    public bool startManually = true;
    public ADJPLogLevel logLevel = ADJPLogLevel.Verbose;
    public ADJPEnvironment environment = ADJPEnvironment.Sandbox;



    //Parameter :   AdjustPurchase
    public string defaultCurrency = "USA";
    public List<AdjustRevenueTracker> listOfAdjustRevenueTracker;

    #endregion

    #region Private Variables

    private string appToken;
    private List<PurchasedItemInfo> m_ListOfActivePurchasedItemEvent;

    #endregion

    #region Mono Behaviour

    private void Awake () {

#if UNITY_IOS
        appToken = appTokenForIOS;
#elif UNITY_ANDROID
        appToken = appTokenForAndroid;
#endif

        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        } else {

            Destroy (gameObject);
        }
        m_ListOfActivePurchasedItemEvent = new List<PurchasedItemInfo> ();

        if (!startManually) {
            Initialization ();
        }
    }

    #endregion

    #region Configuretion   :   AdjustPurchase

    private bool IsValidPreConfigRevenueEvent (int t_EventId) {

        if (t_EventId >= 0 && t_EventId < m_ListOfActivePurchasedItemEvent.Count)
            return true;

        return false;
    }

    private int GetPreConfigRevenueEventIndexFromUniqueIdentifier (string t_UniqueIdentifier){

        int t_NumberOfPreConfigRevenueEvent = m_ListOfActivePurchasedItemEvent.Count;
        for(int i = 0 ; i < t_NumberOfPreConfigRevenueEvent; i++){
            if(t_UniqueIdentifier == listOfAdjustRevenueTracker[i].uniqueIdentifier)
                return i;
        }

        return -1;
    }

    private void RemovePurchaseEventFromActiveList (int t_EventId) {

        if (IsValidPreConfigRevenueEvent (t_EventId)) {

            int t_NumberfActivePurchasedItemEvent = m_ListOfActivePurchasedItemEvent.Count;
            for (int i = t_EventId + 1; i < t_NumberfActivePurchasedItemEvent; i++) {
                m_ListOfActivePurchasedItemEvent[i].eventId--;
            }

            m_ListOfActivePurchasedItemEvent.RemoveAt (t_EventId);
            m_ListOfActivePurchasedItemEvent.TrimExcess ();
        }
    }

    private void VerificationInfoDelegate (ADJPVerificationInfo verificationInfo) {
        Debug.Log (
            "Verification info arrived to unity callback!" +
            ". Message : " + verificationInfo.Message +
            ", Status Code : " + verificationInfo.StatusCode +
            ", Verification State : " + verificationInfo.VerificationState);
    }

    private void TrackRevenue (string t_EventToken, double t_Amount, string t_Currency = "USA") {

        AdjustEvent t_NewAdjustEvent = new AdjustEvent (t_EventToken);
        t_NewAdjustEvent.setRevenue (t_Amount, t_Currency);
        Adjust.trackEvent (t_NewAdjustEvent);
    }

    private void TrackRevenueWithDeDuplication (string t_EventToken, double t_Amount, string t_TransactionID, string t_Currency = "USA") {

        AdjustEvent t_NewAdjustEvent = new AdjustEvent (t_EventToken);
        t_NewAdjustEvent.setRevenue (t_Amount, t_Currency);
        t_NewAdjustEvent.setTransactionId (t_TransactionID);
        Adjust.trackEvent (t_NewAdjustEvent);
    }

    #endregion

    #region Public Callback

    public void Initialization () {

        StartAdjustPurchase ();
    }

    #endregion

    #region Public Callbak  :   AdjustPurchase

    public void StartAdjustPurchase () {

        ADJPConfig config = new ADJPConfig (appToken, environment);
        config.SetLogLevel (logLevel);
        AdjustPurchase.Init (config);
    }
    public void InvokePreConfigRevenueEvent (
        AdjustRevenueEventTrack t_RevenueEventUniqueIdentifier,
        string t_TransactionId = "NULL") {
        
        int t_RevenueEventIndex = GetPreConfigRevenueEventIndexFromUniqueIdentifier (t_RevenueEventUniqueIdentifier.ToString ());
        if (IsValidPreConfigRevenueEvent (t_RevenueEventIndex)){

            InvokePreConfigRevenueEvent (
            t_RevenueEventIndex,
            t_TransactionId);
        }
        
    }

    public void InvokePreConfigRevenueEvent (int t_RevenueEventIndex) {

        if (IsValidPreConfigRevenueEvent (t_RevenueEventIndex)) {

            InvokePreConfigRevenueEvent (
                t_RevenueEventIndex,
                listOfAdjustRevenueTracker[t_RevenueEventIndex].GetTransactionId.Invoke ()
            );
        }
    }

    public void InvokePreConfigRevenueEvent (
        int t_RevenueEventIndex, 
        string t_TransactionId = "NULL") {

        if (IsValidPreConfigRevenueEvent (t_RevenueEventIndex)) {

            string t_RevenueEventToken = "";

#if UNITY_IOS
            t_EventToken = listOfAdjustRevenueTracker[t_RevenueEventIndex].eventTokenForIOS;
#elif UNITY_ANDROID
            t_RevenueEventToken = listOfAdjustRevenueTracker[t_RevenueEventIndex].eventTokenForAndroid;
#endif

            TrackRevenueWithDeDuplication (
                t_RevenueEventToken,
                listOfAdjustRevenueTracker[t_RevenueEventIndex].purchaseValue,
                (t_TransactionId == "NULL" ? listOfAdjustRevenueTracker[t_RevenueEventIndex].GetTransactionId.Invoke () : t_TransactionId),
                listOfAdjustRevenueTracker[t_RevenueEventIndex].currency);
        }
    }

    public void VerifyPurchaseForiOS (
        string t_Recepit,
        string t_TransactionId,
        string t_ProductId) {

        AdjustPurchase.VerifyPurchaseiOS (t_Recepit, t_TransactionId, t_ProductId, VerificationInfoDelegate);
    }

    //Recommanded Approch
    public void VerifyPurchaseForiOSWithEventTracking (
        string t_Recepit,
        string t_TransactionId,
        string t_ProductId,
        double t_PurchasedValue,
        string t_Currency = "",
        UnityAction OnPurchasedSuccessful = null,
        UnityAction OnPurchasedFailed = null,
        UnityAction<string> OnPassingLogEvent = null
    ) {
        PurchasedItemInfo t_NewPurchasedItemInfo = new PurchasedItemInfo (
            m_ListOfActivePurchasedItemEvent.Count + 1,
            environment == ADJPEnvironment.Sandbox ? true : false,
            t_ProductId,
            t_TransactionId,
            t_Currency == "" ? defaultCurrency : t_Currency,
            t_PurchasedValue,
            RemovePurchaseEventFromActiveList,
            OnPurchasedSuccessful,
            OnPurchasedFailed,
            OnPassingLogEvent
        );
        t_NewPurchasedItemInfo.VerifyPurchaseForiOS (t_Recepit, t_TransactionId, t_ProductId);
        m_ListOfActivePurchasedItemEvent.Add (t_NewPurchasedItemInfo);
    }

    public void VerifyPurchaseForAndroid (
        string t_ItemSKU,
        string t_ItemToken,
        string t_DeveloperPayLoad,
        UnityAction OnPurchasedSuccessful = null,
        UnityAction OnPurchasedFailed = null) {

        AdjustPurchase.VerifyPurchaseAndroid (t_ItemSKU, t_ItemToken, t_DeveloperPayLoad, VerificationInfoDelegate);
    }

    //Recommanded Approch
    public void VerifyPurchaseForAndroidWithEventTracking (
        string t_ItemSKU,
        string t_ItemToken,
        string t_DeveloperPayLoad,
        double t_PurchasedValue,
        string t_Currency = "",
        UnityAction OnPurchasedSuccessful = null,
        UnityAction OnPurchasedFailed = null,
        UnityAction<string> OnPassingLogEvent = null
    ) {

        PurchasedItemInfo t_NewPurchasedItemInfo = new PurchasedItemInfo (
            m_ListOfActivePurchasedItemEvent.Count + 1,
            environment == ADJPEnvironment.Sandbox ? true : false,
            t_ItemSKU,
            t_ItemToken,
            t_Currency == "" ? defaultCurrency : t_Currency,
            t_PurchasedValue,
            RemovePurchaseEventFromActiveList,
            OnPurchasedSuccessful,
            OnPurchasedFailed,
            OnPassingLogEvent
        );
        t_NewPurchasedItemInfo.VerifyPurchaseForAndroid (t_ItemSKU, t_ItemToken, t_DeveloperPayLoad);
        m_ListOfActivePurchasedItemEvent.Add (t_NewPurchasedItemInfo);
    }

    #endregion
}